//
//  SignupOrLoginViewController.swift
//  fellows
//
//  Created by chuan Lai  on 2019/10/14.
//  Copyright © 2019 chuan Lai . All rights reserved.
//

import UIKit

class SignupOrLoginViewController: UIViewController{
    
    let signupOrLoginView = SignupOrLoginView()
    override func viewDidLoad() {
        super.viewDidLoad()

        view = signupOrLoginView
        navigationController?.isNavigationBarHidden = true
        signupOrLoginView.phoneSignupOrLoginButton.addTarget(self, action: #selector(handleNext), for: .touchUpInside)
        signupOrLoginView.forgetPasswordButton.addTarget(self, action: #selector(handleForgetPassword), for: .touchUpInside)
    }
    @objc func handleNext(){
        let transferPhoneViewController = TransferPhoneViewController()
        navigationController?.pushViewController(transferPhoneViewController, animated: true)
    }
    @objc func handleForgetPassword(){
        let forgetPasswordTransPhoneController = ForgetPasswordTransPhoneController()
        navigationController?.pushViewController(forgetPasswordTransPhoneController, animated: true)
    }
}
