//
//  File.swift
//  fellows
//
//  Created by chuan Lai  on 2019/10/14.
//  Copyright © 2019 chuan Lai . All rights reserved.
//

import UIKit
class SignupOrLoginView: UIView,UITextFieldDelegate {
    override init(frame: CGRect) {
        super.init(frame: frame)
        accuntTextfield.delegate = self
        passwordTextField.delegate = self
        setupUI()
        setupUIParms()

        }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.endEditing(true)
    }
    
    
        func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
           print("要開始編輯了...")
           
           return true
       }
       func textFieldDidBeginEditing(_ textField: UITextField) {
           print("正在編輯狀態中...")
        
       }
       func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
           print("即將編輯结束...")
           return true
       }
       
       func textFieldDidEndEditing(_ textField: UITextField) {
           print("已經结束編輯狀態...")
       }

       func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
       }
    func setupUI(){
        addSubview(backgroundImageview)
        addSubview(logoImageView)
        addSubview(loginLabel)
        addSubview(accuntTextfield)
        addSubview(passwordTextField)
        addSubview(confirmButton)
        addSubview(forgetPasswordButton)
        addSubview(signupOrLoginStackView)
        signupOrLoginStackView.addArrangedSubview(phoneSignupOrLoginButton)
        signupOrLoginStackView.addArrangedSubview(fbSignupOrLoginButton)
        signupOrLoginStackView.addArrangedSubview(gSignupOrLoginButton)
        addSubview(rigistOrLoginLabel)
        addSubview(divideLine)
        addSubview(policyLabel)
        
        backgroundImageview.topAnchor.constraint(equalTo: topAnchor).isActive = true
        backgroundImageview.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        backgroundImageview.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
        backgroundImageview.rightAnchor.constraint(equalTo: rightAnchor).isActive = true


        logoImageView.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        logoImageView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor, constant: 100).isActive = true
        logoImageView.widthAnchor.constraint(equalToConstant: 161).isActive = true
        logoImageView.heightAnchor.constraint(equalToConstant: 38).isActive = true
        
        loginLabel.topAnchor.constraint(equalTo: logoImageView.bottomAnchor, constant: 15).isActive = true
        loginLabel.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        loginLabel.widthAnchor.constraint(equalToConstant: 67).isActive = true
        loginLabel.heightAnchor.constraint(equalToConstant: 25).isActive = true
        
        accuntTextfield.topAnchor.constraint(equalTo: loginLabel.bottomAnchor, constant: 15).isActive = true
        accuntTextfield.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        accuntTextfield.widthAnchor.constraint(equalToConstant: 249).isActive = true
        accuntTextfield.heightAnchor.constraint(equalToConstant: 45).isActive = true
        
        passwordTextField.topAnchor.constraint(equalTo: accuntTextfield.bottomAnchor, constant: 15).isActive = true
        passwordTextField.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        passwordTextField.widthAnchor.constraint(equalToConstant: 249).isActive = true
        passwordTextField.heightAnchor.constraint(equalToConstant: 45).isActive = true
        
        confirmButton.topAnchor.constraint(equalTo: passwordTextField.bottomAnchor, constant: 15).isActive = true
        confirmButton.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        confirmButton.widthAnchor.constraint(equalToConstant: 249).isActive = true
        confirmButton.heightAnchor.constraint(equalToConstant: 52).isActive = true
        
        forgetPasswordButton.topAnchor.constraint(equalTo: confirmButton.bottomAnchor, constant: 15).isActive = true
        forgetPasswordButton.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        forgetPasswordButton.widthAnchor.constraint(equalToConstant: 249).isActive = true
        forgetPasswordButton.heightAnchor.constraint(equalToConstant: 52).isActive = true
        
        signupOrLoginStackView.topAnchor.constraint(equalTo: forgetPasswordButton.bottomAnchor, constant: 25).isActive = true
        signupOrLoginStackView.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        signupOrLoginStackView.widthAnchor.constraint(equalToConstant: 160).isActive = true
        signupOrLoginStackView.heightAnchor.constraint(equalToConstant: 60).isActive = true
        
        rigistOrLoginLabel.topAnchor.constraint(equalTo: signupOrLoginStackView.bottomAnchor, constant: 15).isActive = true
        rigistOrLoginLabel.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        rigistOrLoginLabel.widthAnchor.constraint(equalToConstant: 54).isActive = true
        rigistOrLoginLabel.heightAnchor.constraint(equalToConstant: 16).isActive = true
        
        divideLine.topAnchor.constraint(equalTo: rigistOrLoginLabel.bottomAnchor, constant: 25).isActive = true
        divideLine.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        divideLine.widthAnchor.constraint(equalToConstant: 180).isActive = true
        divideLine.heightAnchor.constraint(equalToConstant: 1).isActive = true
        
        policyLabel.topAnchor.constraint(equalTo: divideLine.bottomAnchor, constant: 25).isActive = true
        policyLabel.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        policyLabel.widthAnchor.constraint(equalToConstant: 280).isActive = true
        policyLabel.heightAnchor.constraint(equalToConstant: 17).isActive = true
        
    }

    func setupUIParms(){
        loginLabel.font = loginLabel.font.withSize(16)
        loginLabel.textColor = .white
        
        
        let attributes = [
            NSAttributedString.Key.foregroundColor: UIColor.white,
            NSAttributedString.Key.font : UIFont(name: "PingFangSC-Medium", size: 13)!
        ]
        accuntTextfield.layer.cornerRadius = 22
        accuntTextfield.layer.backgroundColor = UIColor(red: 25/255, green: 40/255, blue: 68/255, alpha: 1).cgColor
        
        accuntTextfield.attributedPlaceholder = NSAttributedString(string: "    您的帳號（手機號碼）", attributes:attributes)
        accuntTextfield.keyboardType = .decimalPad
        
        passwordTextField.layer.cornerRadius = 22
        passwordTextField.layer.backgroundColor = UIColor(red: 25/255, green: 40/255, blue: 68/255, alpha: 1).cgColor
        passwordTextField.attributedPlaceholder = NSAttributedString(string: "   請輸入6-20位數位英數密碼", attributes:attributes)
        passwordTextField.isSecureTextEntry = true
        
        confirmButton.layer.cornerRadius = 26
        confirmButton.backgroundColor = UIColor(red: 234/255, green: 84/255, blue: 85/255, alpha: 1)
        confirmButton.setTitleColor(.white, for: .normal)
        forgetPasswordButton.setTitleColor(UIColor(red: 248/255, green: 231/255, blue: 28/255, alpha: 1), for: .normal)

        rigistOrLoginLabel.font = rigistOrLoginLabel.font.withSize(11)
        rigistOrLoginLabel.textColor = .white
        
        policyLabel.font = policyLabel.font.withSize(11)
        policyLabel.textColor = .lightGray
        
        

    }

    let backgroundImageview: UIImageView = {
        return UIImageView.standardAwesomeImageView(title: "login_background")
    }()
    
    let logoImageView: UIImageView = {
        return UIImageView.standardAwesomeImageView(title: "Logo_red_long")
    }()
    let loginLabel: UILabel = {
        return UILabel.standardAwesomeLabel(title: "會員登入")
    }()
    let accuntTextfield: UITextField = {
        return UITextField.standardAwesomeTextfield(placeholder: "")
    }()
    let passwordTextField: UITextField = {
        return UITextField.standardAwesomeTextfield(placeholder: "")
    }()
    let confirmButton: UIButton = {
          return UIButton.standardAwesomeButton(title: "揪團去", imageName: nil)
      }()
    let forgetPasswordButton: UIButton = {
          return UIButton.standardAwesomeButton(title: "忘記密碼", imageName: nil)
      }()
    let signupOrLoginStackView: UIStackView = {
           let sv = UIStackView()
//           sv.spacing = 10
           sv.axis = .horizontal
        sv.distribution = .fillEqually
           sv.translatesAutoresizingMaskIntoConstraints = false
           return sv
       }()
    let phoneSignupOrLoginButton: UIButton = {
          return UIButton.standardAwesomeButton(title: "", imageName: "btn_iphone")
      }()
    let fbSignupOrLoginButton: UIButton = {
          return UIButton.standardAwesomeButton(title: "", imageName: "btn_fb")
      }()
    let gSignupOrLoginButton: UIButton = {
          return UIButton.standardAwesomeButton(title: "", imageName: "btn_google")
      }()
    let rigistOrLoginLabel: UILabel = {
           return UILabel.standardAwesomeLabel(title: "註冊/登入")
       }()
    let divideLine: UIView = {
        let dl = UIView()
        dl.translatesAutoresizingMaskIntoConstraints = false
        dl.backgroundColor = .lightGray
        return dl
    }()
    let policyLabel: UILabel = {
        return UILabel.standardAwesomeLabel(title: "註冊帳號即代表您同意我們的服務條款以及隱私權帳號。")
    }()
    
}


