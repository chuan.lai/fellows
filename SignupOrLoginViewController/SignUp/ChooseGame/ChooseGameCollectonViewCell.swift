//
//  MyCollectionViewCell.swift
//  FiveLakes
//
//  Created by chuan Lai  on 2019/9/23.
//  Copyright © 2019 chuan Lai . All rights reserved.
//

import UIKit

class ChooseGameCollectonViewCell: UICollectionViewCell {


    let chooseLabel: UILabel = {
        let siv = UILabel()
        siv.translatesAutoresizingMaskIntoConstraints = false
        siv.textAlignment = .center
        return siv
    }()
    let chooseBtn: UIImageView = {
           let siv = UIImageView()
        siv.isHidden = true
        siv.image = UIImage(named: "earth")
           siv.translatesAutoresizingMaskIntoConstraints = false
           return siv
       }()
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubview(chooseLabel)
        chooseLabel.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        chooseLabel.rightAnchor.constraint(equalTo: rightAnchor, constant: -15).isActive = true
        chooseLabel.leftAnchor.constraint(equalTo: leftAnchor, constant: 15).isActive = true
        chooseLabel.heightAnchor.constraint(equalToConstant: 30).isActive = true
        chooseLabel.textColor = .white
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}


