//
//  ChooseGameViewController.swift
//  fellows
//
//  Created by chuan Lai  on 2019/10/24.
//  Copyright © 2019 chuan Lai . All rights reserved.
//

import UIKit
class ChooseGameViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate ,UICollectionViewDelegateFlowLayout{


    var selectedArr = [String]()//按的結果
    
    var selectedBoolArr = [Bool]()
    let cellId = "chooseCell"
    let footerCell = "footer"
    let dataArr = ["ewfe","0","1","2","3","4","5","6","7","8","9","0","-","11","22","33","44","55","66","77","88","99","00","--","111","222","333","444","555","666","777","888","999","000","---","1111","2222","3333","4444","5555","6666","7777","8888","9999","0000","----"]
    
    var filterDataList: [String] = [String]()//搜尋結果
    var chooseCollectionView: UICollectionView!

    override func viewDidLoad() {
        super.viewDidLoad()
        let dataCount = dataArr.count
        for _ in 0..<dataCount {
            selectedBoolArr.append(false)
        }
        searchBar.searchBarStyle = .default

        view.backgroundColor = ThemeColor().loginThemeColor
        self.searchBar.delegate = self
        setupUI()
        setupParms()
        setFlowLayout()

        self.filterDataList = self.dataArr

    }
   


    func setFlowLayout() {

          let layout: UICollectionViewFlowLayout = ChooseGameCollectionViewFlowLayout()
          layout.scrollDirection = .vertical
                 layout.estimatedItemSize = UICollectionViewFlowLayout.automaticSize
                 layout.minimumInteritemSpacing = 10
                 layout.minimumLineSpacing = 10
                 layout.sectionInset = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)

                  
                  self.chooseCollectionView = UICollectionView(frame: CGRect(x: 0, y: 180, width: self.view.frame.size.width, height: self.view.frame.size.height-180), collectionViewLayout: layout)
                  self.chooseCollectionView.dataSource = self
                  self.chooseCollectionView.delegate = self
                  self.chooseCollectionView.register(ChooseGameCollectonViewCell.self, forCellWithReuseIdentifier: cellId)
        self.chooseCollectionView.register(ChooseFooter.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionFooter, withReuseIdentifier: footerCell)

                  self.chooseCollectionView.backgroundColor = UIColor.clear
                  self.chooseCollectionView.allowsMultipleSelection = true
                  self.view.addSubview(chooseCollectionView)
    }

    func setupUI(){
        view.addSubview(mainLabel)
        view.addSubview(searchBar)

        mainLabel.topAnchor.constraint(equalTo: view.topAnchor, constant: 80).isActive = true
        mainLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        mainLabel.widthAnchor.constraint(equalToConstant: 120).isActive = true
        mainLabel.heightAnchor.constraint(equalToConstant: 30).isActive = true
        searchBar.topAnchor.constraint(equalTo: mainLabel.bottomAnchor, constant: 10).isActive = true
        searchBar.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        searchBar.widthAnchor.constraint(equalToConstant: view.frame.width-50).isActive = true
        searchBar.heightAnchor.constraint(equalToConstant: 50).isActive = true
       
    }
    
    func setupParms(){
        mainLabel.font = mainLabel.font.withSize(20)
        mainLabel.textColor = .white
        
//        searchBar.showsCancelButton = true
        searchBar.setBackgroundImage(UIImage(), for: .any, barMetrics: .default)
        searchBar.layer.cornerRadius = 25

    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.filterDataList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! ChooseGameCollectonViewCell
//        self.allIndexPath = indexPath
        cell.chooseLabel.text = filterDataList[indexPath.row]
        cell.layer.masksToBounds = true
        cell.layer.cornerRadius = 20
        cell.backgroundColor = UIColor.clear
        cell.layer.borderWidth = 1
        cell.layer.borderColor = UIColor.white.cgColor
        cell.backgroundColor = selectedBoolArr[indexPath.row] ? UIColor(red: 200/256, green: 105/256, blue: 125/256, alpha: 1) : UIColor.clear
        return cell
    }
    func collectionView(_: UICollectionView, layout _: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
           let label = UILabel(frame: CGRect.zero)
           label.font = UIFont.systemFont(ofSize: 20)
           label.text = self.filterDataList[indexPath.row]
           label.sizeToFit()
           let size = label.frame.size
           return CGSize(width: size.width + 80, height: 40)
       }
    func collectionView(_ collectionView: UICollectionView, layout _: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
           return CGSize.zero
       }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
         let selectedCell:UICollectionViewCell = chooseCollectionView.cellForItem(at: indexPath) as! ChooseGameCollectonViewCell
        
        let selectStr = filterDataList[indexPath.row]
        //值對應索引值
        let findIndex = dataArr.firstIndex(where: { (e) -> Bool in
            return (e == selectStr)
        })
        
        guard let valueOfFinalIndex = findIndex else { return }
        print(valueOfFinalIndex)
        print(selectedBoolArr[valueOfFinalIndex])

        switch selectedBoolArr[valueOfFinalIndex] {
        case true:
            if selectedArr.count <= 3{
                let deletStr = filterDataList[indexPath.row]
                print(deletStr)
                    selectedArr = selectedArr.filter(){$0 != deletStr}
                selectedBoolArr[valueOfFinalIndex] = false
                selectedCell.backgroundColor = UIColor.clear
            }

        case false:

              if selectedArr.count < 3{
                 selectedBoolArr[valueOfFinalIndex] = true
                selectedCell
                    .backgroundColor = UIColor(red: 200/256, green: 105/256, blue: 125/256, alpha: 1)
                selectedArr.append(filterDataList[indexPath.row])

              }else{
                let alert = UIAlertController(title: "Alert", message: "No Image Found:", preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "Click", style: UIAlertAction.Style.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
           
        }
//        chooseCollectionView.reloadData()
        print(dataArr,"out")
        print(selectedBoolArr,"out")
        print(selectedArr,"out")
        
    }
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let footer = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: footerCell, for: indexPath) as! ChooseFooter
//        let footer = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "Footer", for: indexPath) as! UICollectionReusableView

        return footer
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        return CGSize(width: view.frame.width, height: 100)
    }

//    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
//    let cellToDeselect:UICollectionViewCell = chooseCollectionView.cellForItem(at: indexPath)!
//    cellToDeselect.contentView.backgroundColor = UIColor.clear
//        let deletStr = dataArr[indexPath.row]
//        selectedArr = selectedArr.filter(){$0 != deletStr}
//
//        selectedBoolArr[indexPath.row] = false
//        print(selectedBoolArr)
//        print(selectedArr)
//
//    }
    let mainLabel: UILabel = {return UILabel.standardAwesomeLabel(title: "我有玩的遊戲")}()
    var searchBar: UISearchBar = {
        let sb = UISearchBar()
        sb.translatesAutoresizingMaskIntoConstraints = false
        return sb
    }()
    
    
}

extension ChooseGameViewController: UISearchBarDelegate{
     
        func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
//            chooseCollectionView.reloadData()
            self.filterDataList = searchText.isEmpty ? self.dataArr : self.dataArr.filter { (temp: String) -> Bool in
                // If dataItem matches the searchText, return true to include it
                return temp.range(of: searchText, options: .forcedOrdering, range: nil, locale: nil) != nil

            }
            
            if self.filterDataList .isEmpty {
                let alert = UIAlertController(title: "Alert", message: "No Image Found:", preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "Click", style: UIAlertAction.Style.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }

            self.chooseCollectionView.reloadData()

        }

    }

