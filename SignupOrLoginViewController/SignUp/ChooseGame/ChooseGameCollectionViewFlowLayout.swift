//
//  StretchyHeaderLayout.swift
//  StretchyHeaderLBTA
//
//  Created by Brian Voong on 12/22/18.
//  Copyright © 2018 Brian Voong. All rights reserved.
//

import UIKit

class ChooseGameCollectionViewFlowLayout: UICollectionViewFlowLayout {
    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
           let attributes = super.layoutAttributesForElements(in: rect)

           var leftMargin = sectionInset.left
           var maxY: CGFloat = -1.0
           attributes?.forEach { layoutAttribute in
               if layoutAttribute.representedElementCategory == .cell {
                   if layoutAttribute.frame.origin.y >= maxY {
                       leftMargin = sectionInset.left
                   }
                   layoutAttribute.frame.origin.x = leftMargin
                   leftMargin += layoutAttribute.frame.width + minimumInteritemSpacing
                   maxY = max(layoutAttribute.frame.maxY, maxY)
               }
           }
           return attributes
       }
      }
    
