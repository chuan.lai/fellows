//
//  ChooseHeader.swift
//  fellows
//
//  Created by chuan Lai  on 2019/10/25.
//  Copyright © 2019 chuan Lai . All rights reserved.
//

import UIKit

class ChooseFooter: UICollectionReusableView {
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .red
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
