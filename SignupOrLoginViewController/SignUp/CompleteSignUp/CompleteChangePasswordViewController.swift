//
//  CompleteChangePasswordViewController.swift
//  fellows
//
//  Created by chuan Lai  on 2019/10/24.
//  Copyright © 2019 chuan Lai . All rights reserved.
//

import UIKit

class CompleteChangePasswordViewController: UIViewController {
    var timer: Timer?

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = ThemeColor().loginThemeColor
            self.setupUI()
            self.setupUIParms()
        self.timer = Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(goUpdate), userInfo: nil, repeats: false)
    }
    @objc func goUpdate(){
        let mainTabBarController = MainTabBarController()
        mainTabBarController.modalPresentationStyle = .fullScreen
        self.present(mainTabBarController, animated: false, completion: nil)
    }
    override func viewDidDisappear(_ animated: Bool) {
         // 將timer的執行緒停止
         if self.timer != nil {
              self.timer?.invalidate()
         }
    }
    func setupUI(){
        view.addSubview(mainImageView)
        view.addSubview(mainLabel)
        mainImageView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 120).isActive = true
        mainImageView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        mainImageView.widthAnchor.constraint(equalToConstant: 183).isActive = true
        mainImageView.heightAnchor.constraint(equalToConstant: 186).isActive = true
        
        mainLabel.topAnchor.constraint(equalTo: mainImageView.bottomAnchor, constant:30).isActive = true
        mainLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        mainLabel.widthAnchor.constraint(equalToConstant: 224).isActive = true
        mainLabel.heightAnchor.constraint(equalToConstant: 50).isActive = true
    }
    func setupUIParms(){
        mainLabel.numberOfLines = 0
        mainLabel.textAlignment = .center
        mainLabel.textColor = .white
    }
 
    let mainImageView: UIImageView={return UIImageView.standardAwesomeImageView(title: "earth")}()
    let mainLabel: UILabel={return UILabel.standardAwesomeLabel(title: "歡迎來到fellows星球\n最罩的遊戲夥伴都在這等著你")}()
}
