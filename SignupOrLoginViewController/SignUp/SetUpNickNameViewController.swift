//
//  SetUpNickNameViewController.swift
//  fellows
//
//  Created by chuan Lai  on 2019/10/24.
//  Copyright © 2019 chuan Lai . All rights reserved.
//

import UIKit

class SetUpNickNameViewController: UIViewController,UITextFieldDelegate {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        setupParms()
        mainTextield.delegate = self
        let recognizer = UITapGestureRecognizer(target: self, action: #selector(self.touch))
        recognizer.numberOfTapsRequired = 1
        recognizer.numberOfTouchesRequired = 1
        view.addGestureRecognizer(recognizer)
    }
    @objc func touch() {
        self.view.endEditing(true)
    }
    
    func setupUI(){
        view.addSubview(backgroundImageView)
        view.addSubview(mainLabel)
        view.addSubview(mainTextield)
        view.addSubview(lineView)
        view.addSubview(countLabel)
        view.addSubview(confirmbutton)
        
        backgroundImageView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        backgroundImageView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        backgroundImageView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        backgroundImageView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        mainLabel.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 120).isActive = true
        mainLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        mainLabel.heightAnchor.constraint(equalToConstant: 248).isActive = true
        mainLabel.heightAnchor.constraint(equalToConstant: 30).isActive = true
        
        mainTextield.topAnchor.constraint(equalTo: mainLabel.bottomAnchor, constant: 20).isActive = true
        mainTextield.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        mainTextield.heightAnchor.constraint(equalToConstant: 39).isActive = true
        mainTextield.heightAnchor.constraint(equalToConstant: 219).isActive = true
        
        lineView.topAnchor.constraint(equalTo: mainTextield.bottomAnchor, constant: 5).isActive = true
        lineView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        lineView.heightAnchor.constraint(equalToConstant: 1).isActive = true
        lineView.widthAnchor.constraint(equalToConstant: 219).isActive = true
        
        countLabel.topAnchor.constraint(equalTo: lineView.bottomAnchor, constant: 2).isActive = true
        countLabel.rightAnchor.constraint(equalTo: lineView.rightAnchor).isActive = true
        countLabel.heightAnchor.constraint(equalToConstant: 16).isActive = true
        countLabel.widthAnchor.constraint(equalToConstant: 40).isActive = true
        
        confirmbutton.topAnchor.constraint(equalTo: lineView.bottomAnchor, constant: 50).isActive = true
        confirmbutton.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        confirmbutton.heightAnchor.constraint(equalToConstant: 58).isActive = true
        confirmbutton.widthAnchor.constraint(equalToConstant: 246).isActive = true
        
    }
    func setupParms(){
        mainLabel.textAlignment = .center
        mainLabel.font = mainLabel.font.withSize(20)
        countLabel.textColor = .lightGray
        let gradientLayer = CAGradientLayer()
               gradientLayer.frame = CGRect(x: 0, y: 0, width: 245, height: 58)
               gradientLayer.colors = [UIColor(red: 49/255, green: 210/255, blue: 241/255, alpha: 1).cgColor,UIColor(red: 77/255, green: 144/255, blue: 254/255, alpha: 1).cgColor]
               //        gradientLayer.locations = [0.0,0.1]
               //縱向
               //        gradientLayer.startPoint = CGPoint(x: 1.0, y: 1.0)
               //        gradientLayer.endPoint = CGPoint(x: 0.0, y: 0.0)
               // 橫向
               gradientLayer.startPoint = CGPoint(x: 0, y: 0)
        gradientLayer.endPoint = CGPoint(x: 1, y: 0)
        confirmbutton.layer.insertSublayer(gradientLayer, at: 0)
        confirmbutton.layer.masksToBounds = true
        confirmbutton.layer.cornerRadius = 29
        confirmbutton.setTitleColor(UIColor.white, for: .normal)
        confirmbutton.addTarget(self, action: #selector(handleNext), for: .touchUpInside)
    }
    @objc func handleNext(){
        
    }
        func setupTextfieldColorChange(type:Int){
               let gradientLayer = CAGradientLayer()
                   gradientLayer.frame = CGRect(x: 0, y: 0, width: 219, height: 1)
            switch type {
                case 0:
                gradientLayer.colors = [UIColor(red: 49/255, green: 210/255, blue: 241/255, alpha: 1).cgColor,UIColor(red: 77/255, green: 144/255, blue: 254/255, alpha: 1).cgColor]
                case 1:
                    gradientLayer.colors = [UIColor.lightGray.cgColor]
            default:
                break
            }
                   gradientLayer.startPoint = CGPoint(x: 0, y: 0)
                   gradientLayer.endPoint = CGPoint(x: 1, y: 0)
            lineView.layer.addSublayer(gradientLayer)
//            countLabel.layer.addSublayer(gradientLayer)
           }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
             print("要開始編輯了...")
             return true
         }
         func textFieldDidBeginEditing(_ textField: UITextField) {
             print("正在編輯狀態中...")
           setupTextfieldColorChange(type: 0)
            countLabel.textColor = UIColor(red: 90/255, green: 152/255, blue: 211/255, alpha: 1)
//            countLabel.text = "(\(textField.)/12)"
//            print(textField.text)

         }
    
         func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
             print("即將編輯结束...")
             return true
         }
         func textFieldDidEndEditing(_ textField: UITextField) {
             print("已經结束編輯狀態...")
          setupTextfieldColorChange(type: 1)
          countLabel.textColor = UIColor.lightGray


         }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
     guard let text = mainTextield.text else { return true }
     let count = text.count + string.count - range.length
        print(count)
        countLabel.text = "(\(count)/12)"
     return count < 12
    }
         func textFieldShouldReturn(_ textField: UITextField) -> Bool {
          textField.resignFirstResponder()
          return true
         }
    let backgroundImageView: UIImageView = {return UIImageView.standardAwesomeImageView(title: "bg_username")}()
    let mainLabel: UILabel = {return UILabel.standardAwesomeLabel(title: "取一個轟動遊戲界的暱稱吧！")}()
    let mainTextield: UITextField = {return UITextField.standardAwesomeTextfield(placeholder: "你希望被夥伴如何稱呼呢？")}()
    let lineView: UIView = {
        let lv = UIView()
        lv.translatesAutoresizingMaskIntoConstraints = false
        lv.backgroundColor = .lightGray
        return lv
    }()
    let countLabel: UILabel = {return UILabel.standardAwesomeLabel(title: "")}()
    let confirmbutton: UIButton = {return UIButton.standardAwesomeButton(title: "確定", imageName: "")}()
    
}
