//
//  SetupPasswordView.swift
//  fellows
//
//  Created by chuan Lai  on 2019/10/17.
//  Copyright © 2019 chuan Lai . All rights reserved.
//

import UIKit

class SetupPasswordView: UIView,UITextFieldDelegate{
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = UIColor(red: 240/255, green: 244/255, blue: 246/255, alpha: 1)
        passwordTextField.delegate = self
        confirmPasswordTextField.delegate = self
        setupUI()
        setupUIParms()
        
        let recognizer = UITapGestureRecognizer(target: self, action: #selector(self.touch))
        recognizer.numberOfTapsRequired = 1
        recognizer.numberOfTouchesRequired = 1
        addGestureRecognizer(recognizer)
    }
    @objc func touch() {
        self.endEditing(true)
    }

//
//    func textFieldShouldReturn(textField: UITextField) -> Bool {
//        // 結束編輯 把鍵盤隱藏起來
//        self.endEditing(true)
//
//        return true
//    }
    func setupTextfieldColorChange(type:Int){
              let gradientLayer = CAGradientLayer()
              gradientLayer.frame = CGRect(x: 0, y: 0, width: 245, height: 1)
           switch type {
               case 0:
               gradientLayer.colors = [UIColor(red: 49/255, green: 210/255, blue: 241/255, alpha: 1).cgColor,UIColor(red: 77/255, green: 144/255, blue: 254/255, alpha: 1).cgColor]
               gradientLayer.startPoint = CGPoint(x: 0, y: 0)
               gradientLayer.endPoint = CGPoint(x: 1, y: 0)
               passwordLine1View.layer.addSublayer(gradientLayer)
               case 1:
                    gradientLayer.colors = [UIColor(red: 49/255, green: 210/255, blue: 241/255, alpha: 1).cgColor,UIColor(red: 77/255, green: 144/255, blue: 254/255, alpha: 1).cgColor]
                    gradientLayer.startPoint = CGPoint(x: 0, y: 0)
                    gradientLayer.endPoint = CGPoint(x: 1, y: 0)
                    passwordLine2View.layer.addSublayer(gradientLayer)
               default:
                   break
           }

        
          }

    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
           print("要開始編輯了...")
           return true
       }
       func textFieldDidBeginEditing(_ textField: UITextField) {
           print("正在編輯狀態中...")
        if textField == passwordTextField{
            setupTextfieldColorChange(type: 0)
        }
        if textField == confirmPasswordTextField{
            setupTextfieldColorChange(type: 1)

        }
        
       }
       func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
           print("即將編輯结束...")
           return true
       }
       func textFieldDidEndEditing(_ textField: UITextField) {
           print("已經结束編輯狀態...")
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = CGRect(x: 0, y: 0, width: 245, height: 1)
        gradientLayer.colors = [UIColor.lightGray.cgColor]
        gradientLayer.startPoint = CGPoint(x: 0, y: 0)
        gradientLayer.endPoint = CGPoint(x: 1, y: 0)
        if textField == passwordTextField{
            passwordLine1View.layer.addSublayer(gradientLayer)
        }
        if textField == confirmPasswordTextField{
            passwordLine2View.layer.addSublayer(gradientLayer)
        }
       }

       func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
       }
    
    func setupUI() {
        addSubview(titleLabel)
        addSubview(titleImageView)
        addSubview(passwordTextField)
        addSubview(passwordLine1View)
        addSubview(confirmPasswordTextField)
        addSubview(passwordLine2View)
        addSubview(sendCertificationButton)


        titleLabel.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor, constant: 120).isActive = true
        titleLabel.leftAnchor.constraint(equalTo: leftAnchor, constant: 80).isActive = true
        titleLabel.heightAnchor.constraint(equalToConstant: 54).isActive = true
        titleLabel.widthAnchor.constraint(equalToConstant: 144).isActive = true
        titleImageView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor, constant: 120).isActive = true
        titleImageView.leftAnchor.constraint(equalTo: titleLabel.rightAnchor, constant: 25).isActive = true
        titleImageView.heightAnchor.constraint(equalToConstant: 74).isActive = true
        titleImageView.widthAnchor.constraint(equalToConstant: 47).isActive = true
        passwordTextField.topAnchor.constraint(equalTo: titleImageView.bottomAnchor, constant: 20).isActive = true
        passwordTextField.centerXAnchor.constraint(equalTo: centerXAnchor, constant: 0).isActive = true
        passwordTextField.heightAnchor.constraint(equalToConstant: 35).isActive = true
        passwordTextField.widthAnchor.constraint(equalToConstant: 245).isActive = true
        
        passwordLine1View.topAnchor.constraint(equalTo: passwordTextField.bottomAnchor, constant: 0).isActive = true
        passwordLine1View.centerXAnchor.constraint(equalTo: centerXAnchor, constant: 0).isActive = true
        passwordLine1View.heightAnchor.constraint(equalToConstant: 1).isActive = true
        passwordLine1View.widthAnchor.constraint(equalToConstant: 245).isActive = true
        
        confirmPasswordTextField.topAnchor.constraint(equalTo: passwordLine1View.bottomAnchor, constant: 20).isActive = true
        confirmPasswordTextField.centerXAnchor.constraint(equalTo: centerXAnchor, constant: 0).isActive = true
        confirmPasswordTextField.heightAnchor.constraint(equalToConstant: 35).isActive = true
        confirmPasswordTextField.widthAnchor.constraint(equalToConstant: 245).isActive = true
               
        passwordLine2View.topAnchor.constraint(equalTo: confirmPasswordTextField.bottomAnchor, constant: 0).isActive = true
        passwordLine2View.centerXAnchor.constraint(equalTo: centerXAnchor, constant: 0).isActive = true
        passwordLine2View.heightAnchor.constraint(equalToConstant: 1).isActive = true
        passwordLine2View.widthAnchor.constraint(equalToConstant: 245).isActive = true
        
        sendCertificationButton.topAnchor.constraint(equalTo: passwordLine2View.bottomAnchor, constant: 25).isActive = true
        sendCertificationButton.centerXAnchor.constraint(equalTo: centerXAnchor, constant: 0).isActive = true
        sendCertificationButton.heightAnchor.constraint(equalToConstant: 58).isActive = true
        sendCertificationButton.widthAnchor.constraint(equalToConstant: 245).isActive = true
        
    }
    
    func setupUIParms() {
        titleLabel.font = titleLabel.font.withSize(15)
        titleLabel.numberOfLines = 0
        titleLabel.textColor = .black
        let passwordAttributes = [
                   NSAttributedString.Key.foregroundColor: UIColor.lightGray,
                   NSAttributedString.Key.font : UIFont(name: "PingFangSC-Medium", size: 15)!
               ]
               passwordTextField.attributedPlaceholder = NSAttributedString(string: "請輸入6-20位英數密碼", attributes:passwordAttributes)
        passwordTextField.textColor = .black
        passwordTextField.keyboardType = .default
        passwordTextField.isSecureTextEntry = true
        let confirmPasswordAttributes = [
                   NSAttributedString.Key.foregroundColor: UIColor.lightGray,
                   NSAttributedString.Key.font : UIFont(name: "PingFangSC-Medium", size: 15)!
               ]
               confirmPasswordTextField.attributedPlaceholder = NSAttributedString(string: "請輸入一次", attributes:confirmPasswordAttributes)
        confirmPasswordTextField.textColor = .black
        confirmPasswordTextField.keyboardType = .default
        confirmPasswordTextField.isSecureTextEntry = true
        let gradientLayer = CAGradientLayer()
               gradientLayer.frame = CGRect(x: 0, y: 0, width: 245, height: 58)
               gradientLayer.colors = [UIColor(red: 49/255, green: 210/255, blue: 241/255, alpha: 1).cgColor,UIColor(red: 77/255, green: 144/255, blue: 254/255, alpha: 1).cgColor]
               //        gradientLayer.locations = [0.0,0.1]
               //縱向
               //        gradientLayer.startPoint = CGPoint(x: 1.0, y: 1.0)
               //        gradientLayer.endPoint = CGPoint(x: 0.0, y: 0.0)
               // 橫向
               gradientLayer.startPoint = CGPoint(x: 0, y: 0)
        gradientLayer.endPoint = CGPoint(x: 1, y: 0)
        sendCertificationButton.layer.insertSublayer(gradientLayer, at: 0)
        sendCertificationButton.layer.masksToBounds = true
        sendCertificationButton.layer.cornerRadius = 29
        sendCertificationButton.setTitleColor(UIColor.white, for: .normal)
        
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    let titleLabel: UILabel = {
        return UILabel.standardAwesomeLabel(title: "設定你的 fellows 密碼")
    }()
    let titleImageView: UIImageView = {
        return UIImageView.standardAwesomeImageView(title: "pic_lock")
    }()
    
    let passwordTextField: UITextField = {
        return UITextField.standardAwesomeTextfield(placeholder: "")
    }()
    let passwordLine1View: UIView = {
        let plv = UIView()
        plv.translatesAutoresizingMaskIntoConstraints = false
        plv.backgroundColor = .lightGray
        return plv
    }()
    let confirmPasswordTextField: UITextField = {
              return UITextField.standardAwesomeTextfield(placeholder: "")
          }()
    let passwordLine2View: UIView = {
        let plv = UIView()
        plv.translatesAutoresizingMaskIntoConstraints = false
        plv.backgroundColor = .lightGray
        return plv
        }()
    let sendCertificationButton: UIButton = {
        return UIButton.standardAwesomeButton(title: "傳送驗證碼", imageName: "")
    }()
    
}





