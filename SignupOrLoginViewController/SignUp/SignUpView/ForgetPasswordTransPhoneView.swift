//
//  ForgetPasswordTransPhoneView.swift
//  fellows
//
//  Created by chuan Lai  on 2019/10/22.
//  Copyright © 2019 chuan Lai . All rights reserved.
//



import UIKit

class ForgetPasswordTransPhoneView: UIView,UITextFieldDelegate {
    override init(frame: CGRect) {
        super.init(frame: frame)
        phoneTextField.delegate = self
        setupUI()
        setupUIParms()
        
        
    }
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.endEditing(true)
    }
    
    func setupUI() {
        addSubview(backgroundImageView)
        addSubview(titleLabel)
        addSubview(titleImageView)
        addSubview(phoneTextField)
        addSubview(phoneLineView)
        addSubview(sendCertificationButton)
        backgroundImageView.topAnchor.constraint(equalTo: topAnchor).isActive = true
        backgroundImageView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        backgroundImageView.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
        backgroundImageView.rightAnchor.constraint(equalTo: rightAnchor).isActive = true

        titleLabel.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor, constant: 100).isActive = true
        titleLabel.leftAnchor.constraint(equalTo: leftAnchor, constant: 80).isActive = true
        titleLabel.heightAnchor.constraint(equalToConstant: 80).isActive = true
        titleLabel.widthAnchor.constraint(equalToConstant: 160).isActive = true
        titleImageView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor, constant: 100).isActive = true
        titleImageView.leftAnchor.constraint(equalTo: titleLabel.rightAnchor, constant: 10).isActive = true
        titleImageView.heightAnchor.constraint(equalToConstant: 98).isActive = true
        titleImageView.widthAnchor.constraint(equalToConstant: 98).isActive = true
        phoneTextField.topAnchor.constraint(equalTo: titleImageView.bottomAnchor, constant: 20).isActive = true
        phoneTextField.centerXAnchor.constraint(equalTo: centerXAnchor, constant: 0).isActive = true
        phoneTextField.heightAnchor.constraint(equalToConstant: 35).isActive = true
        phoneTextField.widthAnchor.constraint(equalToConstant: 245).isActive = true
        
        phoneLineView.topAnchor.constraint(equalTo: phoneTextField.bottomAnchor, constant: 0).isActive = true
        phoneLineView.centerXAnchor.constraint(equalTo: centerXAnchor, constant: 0).isActive = true
        phoneLineView.heightAnchor.constraint(equalToConstant: 1).isActive = true
        phoneLineView.widthAnchor.constraint(equalToConstant: 245).isActive = true
        
        sendCertificationButton.topAnchor.constraint(equalTo: phoneTextField.bottomAnchor, constant: 25).isActive = true
        sendCertificationButton.centerXAnchor.constraint(equalTo: centerXAnchor, constant: 0).isActive = true
        sendCertificationButton.heightAnchor.constraint(equalToConstant: 58).isActive = true
        sendCertificationButton.widthAnchor.constraint(equalToConstant: 245).isActive = true
        
    }
    
    func setupUIParms() {
        titleLabel.font = titleLabel.font.withSize(15)
        titleLabel.numberOfLines = 0
        titleLabel.contentMode = .left
        titleLabel.textColor = .black
        let phoneAttributes = [
                   NSAttributedString.Key.foregroundColor: UIColor.lightGray,
                   NSAttributedString.Key.font : UIFont(name: "PingFangSC-Medium", size: 15)!
               ]
               phoneTextField.attributedPlaceholder = NSAttributedString(string: "請輸入你的手機號碼", attributes:phoneAttributes)
        phoneTextField.keyboardType = .decimalPad
        phoneTextField.textColor = .black
        
        let gradientLayer = CAGradientLayer()
               gradientLayer.frame = CGRect(x: 0, y: 0, width: 245, height: 58)
               gradientLayer.colors = [UIColor(red: 49/255, green: 210/255, blue: 241/255, alpha: 1).cgColor,UIColor(red: 77/255, green: 144/255, blue: 254/255, alpha: 1).cgColor]
               //        gradientLayer.locations = [0.0,0.1]
               //縱向
               //        gradientLayer.startPoint = CGPoint(x: 1.0, y: 1.0)
               //        gradientLayer.endPoint = CGPoint(x: 0.0, y: 0.0)
               // 橫向
               gradientLayer.startPoint = CGPoint(x: 0, y: 0)
        gradientLayer.endPoint = CGPoint(x: 1, y: 0)
        sendCertificationButton.layer.insertSublayer(gradientLayer, at: 0)
        sendCertificationButton.layer.masksToBounds = true
        sendCertificationButton.layer.cornerRadius = 29
        sendCertificationButton.setTitleColor(UIColor.white, for: .normal)
        
    }
        func setupTextfieldColorChange(type:Int){
               let gradientLayer = CAGradientLayer()
                   gradientLayer.frame = CGRect(x: 0, y: 0, width: 245, height: 1)
            switch type {
                case 0:
                gradientLayer.colors = [UIColor(red: 49/255, green: 210/255, blue: 241/255, alpha: 1).cgColor,UIColor(red: 77/255, green: 144/255, blue: 254/255, alpha: 1).cgColor]
                case 1:
                    gradientLayer.colors = [UIColor.lightGray.cgColor]
            default:
                break
            }
                   gradientLayer.startPoint = CGPoint(x: 0, y: 0)
                   gradientLayer.endPoint = CGPoint(x: 1, y: 0)
            phoneLineView.layer.addSublayer(gradientLayer)
    //                  phoneLineView.layer.insertSublayer(gradientLayer, at: 0)
           }

    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
           print("要開始編輯了...")
           return true
       }
       func textFieldDidBeginEditing(_ textField: UITextField) {
           print("正在編輯狀態中...")
         setupTextfieldColorChange(type: 0)
       }
   
       func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
           print("即將編輯结束...")
           return true
       }
       func textFieldDidEndEditing(_ textField: UITextField) {
           print("已經结束編輯狀態...")
        setupTextfieldColorChange(type: 1)

       }

       func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
       }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    let backgroundImageView:UIImageView = {
        return UIImageView.standardAwesomeImageView(title: "BG_lightblue_gd")
    }()
    
    let titleLabel: UILabel = {
        return UILabel.standardAwesomeLabel(title: "忘記密碼了媽？\n我們會傳送驗證碼給你,\n讓你重新設定新密碼")
    }()
    let titleImageView: UIImageView = {
        return UIImageView.standardAwesomeImageView(title: "pic_forgetmima")
    }()
    
    let phoneTextField: UITextField = {
        return UITextField.standardAwesomeTextfield(placeholder: "")
    }()
    let phoneLineView: UIView = {
        let plv = UIView()
        plv.translatesAutoresizingMaskIntoConstraints = false
        plv.backgroundColor = .lightGray
        return plv
    }()
    let sendCertificationButton: UIButton = {
        return UIButton.standardAwesomeButton(title: "傳送驗證碼", imageName: "")
    }()
    
}

