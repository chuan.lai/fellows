//
//  SetupPasswordViewController.swift
//  fellows
//
//  Created by chuan Lai  on 2019/10/17.
//  Copyright © 2019 chuan Lai . All rights reserved.
//

import UIKit

class SetupPasswordViewController: UIViewController {


    let setupPasswordView = SetupPasswordView()

    override func viewDidLoad() {
        super.viewDidLoad()
        view.addSubview(setupPasswordView)
        
        setupPasswordView.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)
        navigationController?.isNavigationBarHidden = false
        let leftButton = UIButton()
        leftButton.addTarget(self, action: #selector(backClick), for: .touchUpInside)
        leftButton.setImage(UIImage(named: "_grey"), for: .normal)
        let leftBarBtn = UIBarButtonItem(customView: leftButton)
        navigationItem.leftBarButtonItem = leftBarBtn
        
        let image = UIImage()
        self.navigationController?.navigationBar.setBackgroundImage(image, for: .default)
        self.navigationController?.navigationBar.shadowImage = image
        self.navigationController?.navigationBar.isTranslucent = true
        setupPasswordView.sendCertificationButton.addTarget(self, action: #selector(handleNext), for: .touchUpInside)
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    @objc func backClick(){
        navigationController?.popViewController(animated: true)
    }

    @objc func handleNext(){
        view.endEditing(true)
        let customAlertController = CustomAlertController()
        let completeRegistViewController = CompleteRegistViewController()
        customAlertController.modalPresentationStyle = .fullScreen
        if setupPasswordView.confirmPasswordTextField.text == setupPasswordView.passwordTextField.text && setupPasswordView.confirmPasswordTextField.text != ""&&setupPasswordView.passwordTextField.text != "" {
             customAlertController.setupUI(imageString: "pic_alert_success", titleString: "註冊成功", contentString: "恭喜您成為fellows好夥伴的一員🥳", buttonColor: UIColor(red: 160/255, green: 201/255, blue: 94/255, alpha: 1), buttonString: "好", alertStatus: .confirm, orginalController: SetupPasswordViewController(), controllerToPresent: completeRegistViewController)

        }else{
        customAlertController.setupUI(imageString:"pic_alert_error",titleString:"密碼錯誤",contentString:"可能您的密碼輸入不一致,或大小寫輸入有差異",buttonColor:UIColor(red: 1, green: 138/255, blue: 135/255, alpha: 1),buttonString:"好",alertStatus: .cancel,orginalController: SetupPasswordViewController(),controllerToPresent:nil)
        }
        self.present(customAlertController, animated: false, completion: nil)
    }
}



