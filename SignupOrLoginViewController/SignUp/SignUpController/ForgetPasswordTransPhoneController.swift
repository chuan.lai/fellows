//
//  ForgetPasswordTransPhoneController.swift
//  fellows
//
//  Created by chuan Lai  on 2019/10/22.
//  Copyright © 2019 chuan Lai . All rights reserved.
//



import UIKit

class ForgetPasswordTransPhoneController: UIViewController {
    let forgetPasswordTransPhoneView = ForgetPasswordTransPhoneView()
    override func viewDidLoad() {
        super.viewDidLoad()
        view.addSubview(forgetPasswordTransPhoneView)
        forgetPasswordTransPhoneView.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)
        navigationController?.isNavigationBarHidden = false
        let leftButton = UIButton()
        leftButton.addTarget(self, action: #selector(backClick), for: .touchUpInside)
        leftButton.setImage(UIImage(named: "_grey"), for: .normal)
        let leftBarBtn = UIBarButtonItem(customView: leftButton)
        let spacer = UIBarButtonItem(barButtonSystemItem: .fixedSpace, target: nil,
                                     action: nil)
        spacer.width = -50
        self.navigationItem.leftBarButtonItems = [spacer,leftBarBtn]
        
        let image = UIImage()
        self.navigationController?.navigationBar.setBackgroundImage(image, for: .default)
        self.navigationController?.navigationBar.shadowImage = image
        self.navigationController?.navigationBar.isTranslucent = true
       
        forgetPasswordTransPhoneView.sendCertificationButton.addTarget(self, action: #selector(handleNext), for: .touchUpInside)
    }
    @objc func handleNext(){
        view.endEditing(true)
        let resetPasswordViewController = ResetPasswordViewController()
        navigationController?.pushViewController(resetPasswordViewController, animated: true)
    }
    @objc func backClick(){
        navigationController?.popViewController(animated: true)
    }
}



