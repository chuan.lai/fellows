//
//  CustomAlertController.swift
//  fellows
//
//  Created by chuan Lai  on 2019/10/22.
//  Copyright © 2019 chuan Lai . All rights reserved.
//

import Foundation
import UIKit
    enum SelectAlertButtonStatus{
        case cancel
        case confirm
    }
    class CustomAlertController: UIViewController {
    let backgroundView = UIView()
    let alertView = UIView()
    let titleLabel = UILabel()
    let contentLabel = UILabel()
    let alertImageView = UIImageView()
    let alertButton = UIButton(type: .system)
    var alertStatus: SelectAlertButtonStatus? = nil
    var controllerToPresent: UIViewController? = nil
        
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        view.backgroundColor = .clear
        
    }
        func setupUI(imageString:String,titleString:String,contentString:String,buttonColor:UIColor,buttonString:String,alertStatus:SelectAlertButtonStatus,orginalController:UIViewController,controllerToPresent:UIViewController?){
        self.alertStatus = alertStatus
            if controllerToPresent != nil {
                self.controllerToPresent = controllerToPresent
            }
        view.addSubview(orginalController.view)
        alertImageView.translatesAutoresizingMaskIntoConstraints = false
        alertImageView.image = UIImage(named: imageString)

        backgroundView.translatesAutoresizingMaskIntoConstraints = false
        backgroundView.tag = 2222
        backgroundView.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        backgroundView.translatesAutoresizingMaskIntoConstraints = false

        alertView.backgroundColor = UIColor.white.withAlphaComponent(1)
        alertView.layer.cornerRadius = 10.0
        alertView.layer.masksToBounds = true
        alertView.translatesAutoresizingMaskIntoConstraints = false

        titleLabel.numberOfLines = 0
        titleLabel.lineBreakMode = .byWordWrapping
        titleLabel.textColor = UIColor.black
        titleLabel.layer.cornerRadius = 10.0
        titleLabel.textAlignment = .center
        titleLabel.font = UIFont.systemFont(ofSize: 16.0)
        titleLabel.layer.masksToBounds = true
        titleLabel.text = titleString
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
     
        contentLabel.translatesAutoresizingMaskIntoConstraints = false
        contentLabel.numberOfLines = 0
        contentLabel.lineBreakMode = .byWordWrapping
        contentLabel.textColor = UIColor.lightGray
        contentLabel.layer.cornerRadius = 10.0
        contentLabel.textAlignment = .center
        contentLabel.font = UIFont.systemFont(ofSize: 12.0)
        contentLabel.layer.masksToBounds = true
        contentLabel.text = contentString
        contentLabel.translatesAutoresizingMaskIntoConstraints = false
       
        alertButton.translatesAutoresizingMaskIntoConstraints = false
        alertButton.setTitle(buttonString, for: .normal)
        alertButton.backgroundColor = buttonColor
        alertButton.layer.cornerRadius = 21
        alertButton.setTitleColor(.white, for: .normal)

        self.view.addSubview(backgroundView)
        backgroundView.addSubview(alertView)
        alertView.addSubview(alertImageView)
        alertView.addSubview(titleLabel)
        alertView.addSubview(contentLabel)
        alertView.addSubview(alertButton)


        backgroundView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        backgroundView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        backgroundView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        backgroundView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true

        alertView.centerYAnchor.constraint(equalTo: self.view.centerYAnchor).isActive = true
        alertView.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        alertView.widthAnchor.constraint(equalToConstant: 259).isActive = true
        alertView.heightAnchor.constraint(equalToConstant: 275).isActive = true

        alertImageView.topAnchor.constraint(equalTo: alertView.topAnchor,constant: 25).isActive = true
        alertImageView.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        alertImageView.widthAnchor.constraint(equalToConstant: 70).isActive = true
        alertImageView.heightAnchor.constraint(equalToConstant: 70).isActive = true

        titleLabel.topAnchor.constraint(equalTo: alertImageView.bottomAnchor,constant: 5).isActive = true
        titleLabel.centerXAnchor.constraint(equalTo: alertView.centerXAnchor).isActive = true
        titleLabel.widthAnchor.constraint(equalToConstant: 120).isActive = true
        titleLabel.heightAnchor.constraint(equalToConstant: 30).isActive = true

        contentLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor,constant: 0).isActive = true
        contentLabel.centerXAnchor.constraint(equalTo: alertView.centerXAnchor).isActive = true
        contentLabel.widthAnchor.constraint(equalToConstant: 200).isActive = true
        contentLabel.heightAnchor.constraint(equalToConstant: 30).isActive = true

        alertButton.topAnchor.constraint(equalTo: contentLabel.bottomAnchor,constant: 25).isActive = true
        alertButton.centerXAnchor.constraint(equalTo: alertView.centerXAnchor).isActive = true
        alertButton.widthAnchor.constraint(equalToConstant: 93).isActive = true
        alertButton.heightAnchor.constraint(equalToConstant: 42).isActive = true
        alertButton.addTarget(self, action: #selector(buttonTapped), for: .touchUpInside)
    }
        @objc func buttonTapped(){
            switch alertStatus {
            case .cancel:
                self.dismiss(animated: false, completion: nil)
            case .confirm:
                if self.controllerToPresent != nil{
                    controllerToPresent!.modalPresentationStyle = .fullScreen
                    self.present(controllerToPresent!, animated: true, completion: nil)
                }else{
                    self.dismiss(animated: false, completion: nil)
                }
            default:
                break
            }
        }
}


