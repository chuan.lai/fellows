//
//  extension.swift
//  fellows
//
//  Created by chuan Lai  on 2019/10/14.
//  Copyright © 2019 chuan Lai . All rights reserved.
//

import UIKit

extension UILabel{

            class func standardAwesomeLabel(title: String) -> UILabel {
                let label = UILabel()
                label.font = UIFont(name: "PingFangSC-Medium", size: 14)
//                label.textColor = .black
                label.text = title
//                label.textAlignment = .left
                label.translatesAutoresizingMaskIntoConstraints = false
                return label
            }
        }
extension UIImageView{
            class func standardAwesomeImageView(title: String) -> UIImageView {
                let imageView = UIImageView()
                imageView.image = UIImage(named: title)
                imageView.translatesAutoresizingMaskIntoConstraints = false
                return imageView
            }
        }
extension UITextField{
    class func standardAwesomeTextfield(placeholder: String) -> UITextField {
        let textField = UITextField()
        textField.placeholder = placeholder
        textField.translatesAutoresizingMaskIntoConstraints = false
        return textField
    }
}
extension UIButton {
    
    class func standardAwesomeButton(title: String?,imageName:String?) -> UIButton {
        
        let button = UIButton(type: .system)
//        button.setBackgroundImage(UIImage(named: imageName ?? ""), for: .normal)
        button.setImage(UIImage(named: imageName ?? "")?.withRenderingMode(.alwaysOriginal), for: .normal)
        button.setTitle(title ?? "", for: .normal)
        
        button.translatesAutoresizingMaskIntoConstraints = false
        
        return button
    }
    enum ButtonImagePosition : Int{
        
        case PositionTop = 0
        case Positionleft
        case PositionBottom
        case PositionRight
        case center
    }
    
   func setImageAndTitle(imageName:String,title:String,type:ButtonImagePosition,Space space:CGFloat)  {
       
       self.setTitle(title, for: .normal)
       self.setImage(UIImage(named:imageName), for: .normal)
       
       let imageWith :CGFloat = (self.imageView?.frame.size.width)!;
       let imageHeight :CGFloat = (self.imageView?.frame.size.height)!;
       
       var labelWidth :CGFloat = 0.0;
       var labelHeight :CGFloat = 0.0;
       
       labelWidth = CGFloat(self.titleLabel!.intrinsicContentSize.width);
       labelHeight = CGFloat(self.titleLabel!.intrinsicContentSize.height);
       
       var  imageEdgeInsets :UIEdgeInsets = UIEdgeInsets();
       var  labelEdgeInsets :UIEdgeInsets = UIEdgeInsets();
       
       switch type {
       case .PositionTop:
           imageEdgeInsets = UIEdgeInsets(top: -labelHeight - space/2.0, left: 0, bottom: 0, right: -labelWidth);
           labelEdgeInsets = UIEdgeInsets(top: 0, left: -imageWith, bottom: -imageHeight-space/2.0, right: 0);
           break;
       case .Positionleft:
           imageEdgeInsets = UIEdgeInsets(top: 0, left: -space/2.0, bottom: 0, right: space/2.0);
           labelEdgeInsets = UIEdgeInsets(top: 0, left: space/2.0, bottom: 0, right: -space/2.0);
           break;
       case .PositionBottom:
           imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: -labelHeight-space/2.0, right: -labelWidth);
           labelEdgeInsets = UIEdgeInsets(top: -imageHeight-space/2.0, left: -imageWith, bottom: 0, right: 0);
           break;
       case .PositionRight:
           imageEdgeInsets = UIEdgeInsets(top: 0, left: labelWidth+space/2.0, bottom: 0, right: -labelWidth-space/2.0);
           labelEdgeInsets = UIEdgeInsets(top: 0, left: -imageWith-space/2.0, bottom: 0, right: imageWith+space/2.0);
           break;
       case .center:
           imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0);
           labelEdgeInsets = UIEdgeInsets(top: 0, left: -space*5, bottom: 0, right: 0);
           break;
       }
       
       // 4. 赋值
       self.titleEdgeInsets = labelEdgeInsets;
       self.imageEdgeInsets = imageEdgeInsets;
   }
}
