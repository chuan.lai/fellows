//
//  ShowAlert.swift
//  fellows
//
//  Created by chuan Lai  on 2019/10/18.
//  Copyright © 2019 chuan Lai . All rights reserved.
//

import Foundation
import  UIKit


extension UIView{

    func showToast(text: String){
        
        self.hideToast()
        let toastLb = UILabel()
        toastLb.numberOfLines = 0
        toastLb.lineBreakMode = .byWordWrapping
        toastLb.backgroundColor = UIColor.black.withAlphaComponent(0.7)
        toastLb.textColor = UIColor.white
        toastLb.layer.cornerRadius = 10.0
        toastLb.textAlignment = .center
        toastLb.font = UIFont.systemFont(ofSize: 15.0)
        toastLb.text = text
        toastLb.layer.masksToBounds = true
        toastLb.tag = 9999//tag：hideToast實用來判斷要remove哪個label
        
        let maxSize = CGSize(width: self.bounds.width - 40, height: self.bounds.height)
        var expectedSize = toastLb.sizeThatFits(maxSize)
        var lbWidth = maxSize.width
        var lbHeight = maxSize.height
        if maxSize.width >= expectedSize.width{
            lbWidth = expectedSize.width
        }
        if maxSize.height >= expectedSize.height{
            lbHeight = expectedSize.height
        }
        expectedSize = CGSize(width: lbWidth, height: lbHeight)
        toastLb.frame = CGRect(x: ((self.bounds.size.width)/2) - ((expectedSize.width + 20)/2), y: self.bounds.height - expectedSize.height - 40 - 20, width: expectedSize.width + 20, height: expectedSize.height + 20)
        self.addSubview(toastLb)
        
        UIView.animate(withDuration: 3.0, delay: 1.5, animations: {
            toastLb.alpha = 0.0
        }) { (complete) in
            toastLb.removeFromSuperview()
        }
    }
    
    func hideToast(){
        for view in self.subviews{
            if view is UILabel , view.tag == 9999{
                view.removeFromSuperview()
            }
        }
    }
    //        UIApplication.shared.keyWindow?.showToast(text: "這是一個Toast的範例")

    
    
    
    
    
    
//    func showAlert(name:String,buttonString:String,buttonColor:UIColor,titleString:String,contentString:String,alertStatus: SelectAlertButtonStatus){
//        self.hideAlert()
//        let backgroundView = UIView()
//        let alertView = UIView()
//        let titleLabel = UILabel()
//        let contentLabel = UILabel()
//        
//        
//
//        let alertImageView = UIImageView()
//        let alertButton = UIButton(type: .system)
//
//        alertImageView.translatesAutoresizingMaskIntoConstraints = false
//        alertImageView.image = UIImage(named: name)
//        
//        backgroundView.translatesAutoresizingMaskIntoConstraints = false
//        backgroundView.tag = 2222
//        backgroundView.backgroundColor = UIColor.black.withAlphaComponent(0.6)
//        backgroundView.translatesAutoresizingMaskIntoConstraints = false
//
//        alertView.backgroundColor = UIColor.white.withAlphaComponent(1)
//        alertView.layer.cornerRadius = 10.0
//        alertView.layer.masksToBounds = true
//        alertView.translatesAutoresizingMaskIntoConstraints = false
//        
//        titleLabel.numberOfLines = 0
//        titleLabel.lineBreakMode = .byWordWrapping
//        titleLabel.textColor = UIColor.black
//        titleLabel.layer.cornerRadius = 10.0
//        titleLabel.textAlignment = .center
//        titleLabel.font = UIFont.systemFont(ofSize: 16.0)
//        titleLabel.layer.masksToBounds = true
//        titleLabel.text = titleString
//        titleLabel.translatesAutoresizingMaskIntoConstraints = false
//        let maxSize = CGSize(width: self.bounds.width - 40, height: self.bounds.height)
//              var expectedSize = titleLabel.sizeThatFits(maxSize)
//              var lbWidth = maxSize.width
//              var lbHeight = maxSize.height
//              if maxSize.width >= expectedSize.width{
//                  lbWidth = expectedSize.width
//              }
//              if maxSize.height >= expectedSize.height{
//                  lbHeight = expectedSize.height
//              }
//              expectedSize = CGSize(width: lbWidth, height: lbHeight)
//              titleLabel.frame = CGRect(x: ((self.bounds.size.width)/2) - ((expectedSize.width + 20)/2), y: self.bounds.height - expectedSize.height - 40 - 20, width: expectedSize.width + 20, height: expectedSize.height + 20)
//
//        contentLabel.translatesAutoresizingMaskIntoConstraints = false
//        contentLabel.numberOfLines = 0
//        contentLabel.lineBreakMode = .byWordWrapping
//        contentLabel.textColor = UIColor.lightGray
//        contentLabel.layer.cornerRadius = 10.0
//        contentLabel.textAlignment = .center
//        contentLabel.font = UIFont.systemFont(ofSize: 12.0)
//        contentLabel.layer.masksToBounds = true
//        contentLabel.text = contentString
//        contentLabel.translatesAutoresizingMaskIntoConstraints = false
//        let maxSize2 = CGSize(width: self.bounds.width - 40, height: self.bounds.height)
//              var expectedSize2 = contentLabel.sizeThatFits(maxSize)
//              var lbWidth2 = maxSize.width
//              var lbHeight2 = maxSize.height
//              if maxSize2.width >= expectedSize2.width{
//                  lbWidth2 = expectedSize2.width
//              }
//              if maxSize2.height >= expectedSize2.height{
//                  lbHeight2 = expectedSize2.height
//              }
//              expectedSize2 = CGSize(width: lbWidth2, height: lbHeight2)
//              contentLabel.frame = CGRect(x: ((self.bounds.size.width)/2) - ((expectedSize2.width + 20)/2), y: self.bounds.height - expectedSize2.height - 40 - 20, width: expectedSize2.width + 20, height: expectedSize2.height + 20)
//        
//        alertButton.translatesAutoresizingMaskIntoConstraints = false
//        alertButton.setTitle(buttonString, for: .normal)
//        alertButton.backgroundColor = buttonColor
//        alertButton.layer.cornerRadius = 21
//        alertButton.setTitleColor(.white, for: .normal)
//
//        self.addSubview(backgroundView)
//        backgroundView.addSubview(alertView)
//        alertView.addSubview(alertImageView)
//        alertView.addSubview(titleLabel)
//        alertView.addSubview(contentLabel)
//        alertView.addSubview(alertButton)
//
//        
//        backgroundView.topAnchor.constraint(equalTo: topAnchor).isActive = true
//        backgroundView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
//        backgroundView.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
//        backgroundView.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
//
//        alertView.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
//        alertView.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
//        alertView.widthAnchor.constraint(equalToConstant: 259).isActive = true
//        alertView.heightAnchor.constraint(equalToConstant: 275).isActive = true
//        
//        alertImageView.topAnchor.constraint(equalTo: alertView.topAnchor,constant: 25).isActive = true
//        alertImageView.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
//        alertImageView.widthAnchor.constraint(equalToConstant: 70).isActive = true
//        alertImageView.heightAnchor.constraint(equalToConstant: 70).isActive = true
//
//        titleLabel.topAnchor.constraint(equalTo: alertImageView.bottomAnchor,constant: 5).isActive = true
//        titleLabel.centerXAnchor.constraint(equalTo: alertView.centerXAnchor).isActive = true
//        titleLabel.widthAnchor.constraint(equalToConstant: expectedSize.width + 20).isActive = true
//        titleLabel.heightAnchor.constraint(equalToConstant: expectedSize.height + 20).isActive = true
//        
//        contentLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor,constant: 0).isActive = true
//        contentLabel.centerXAnchor.constraint(equalTo: alertView.centerXAnchor).isActive = true
//        contentLabel.widthAnchor.constraint(equalToConstant: expectedSize2.width + 20).isActive = true
//        contentLabel.heightAnchor.constraint(equalToConstant: expectedSize2.height + 20).isActive = true
//        
//        alertButton.topAnchor.constraint(equalTo: contentLabel.bottomAnchor,constant: 10).isActive = true
//        alertButton.centerXAnchor.constraint(equalTo: alertView.centerXAnchor).isActive = true
//        alertButton.widthAnchor.constraint(equalToConstant: 93).isActive = true
//        alertButton.heightAnchor.constraint(equalToConstant: 42).isActive = true
//        alertButton.addTarget(self, action: #selector(buttonTapped), for: .touchUpInside)
//
////        UIView.animate(withDuration: 1.5, delay: 1.5, animations: {
////            alertView.alpha = 0.0
////        }) { (complete) in
////            alertView.removeFromSuperview()
////        }
//    }
//   
//    func hideAlert(){
//        for view in self.subviews{
//            if view.tag == 2222{
//                view.removeFromSuperview()
//            }
//        }
//    }    
//    @objc func buttonTapped() {
//        hideAlert()
//   }
}
