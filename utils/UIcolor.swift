//
//  UIcolor.swift
//  fellows
//
//  Created by chuan Lai  on 2019/10/16.
//  Copyright © 2019 chuan Lai . All rights reserved.
//

import Foundation
import UIKit

struct ThemeColor{
    let loginThemeColor: UIColor = UIColor(red: 28/255, green: 50/255, blue: 87/255, alpha: 1)
}
